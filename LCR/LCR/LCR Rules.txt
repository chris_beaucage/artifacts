                  ***  LCR RULES  ***

          This game requires 3 or more players.

Each player begins with 3 chips. At the beginning of your turn,
start by rolling the same amount of dice as your total of chips,
with 3 being the maximum. Each die has an L, C, R, and 3 dots (*).
For each L you throw, pass one chip to the player to your left.
For each C you throw, place one chip in the center pot. For each R
you throw, pass one chip to the player to your right. If you don't
have any chips at the start of your turn, your turn is skipped. The
final player remaining with at least one chip wins the game.

#include <iostream>
#include <string>
#include <fstream>
#include "GameRun.h"

using namespace std;

//Read game rules to user.
void Game::StartGame() {
	inFile.open("LCR Rules.txt");
	if (inFile.is_open()) {
		while (getline(inFile, lineFile)) {
			cout << lineFile << '\n';
		}
		inFile.close();
	}

	//Used a do/while loop to ensure the game asks for players once, then repeats if there is an error.
	do {
		cout << "\nEnter number of players: ";
		cin >> numPlayers;
		 
		//Bug fix: Program would repeat error message infinitely without the use of cin.good and cin.fail.
		if (numPlayers < 3 && cin.good()) {
			cout << "Error: At least 3 people are required to play this game." << endl;
		}

		else if (cin.fail()) {
			cout << "Error: Not a recognized number." << endl;
			cin.clear();
			cin.ignore(256, '\n');
		}
	} while (numPlayers < 3 || cin.fail());

	//Now that the number of players have been entered, calculate total chips in the game.
	chipsTotal = numPlayers * 3;
}
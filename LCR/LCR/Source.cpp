#include <iostream>
#include <string>
#include <fstream>
#include "GameRun.h"
#include "GameDice.h"
#include <ctime>
#include <windows.h>

using namespace std;

Game game;
Dice roll;

int main()
{
	//Initialize random number generator.
	srand(time (NULL));

	//Display game rules and request number of players.
	game.StartGame(); 
	
	//Cycle through the number of players and enter their names.
	for (int playerNum = 1; playerNum <= game.numPlayers; playerNum++)
	{
		cout << "Enter first name of player " << playerNum << ": ";
		cin >> game.name[game.player];
		roll.chips[roll.x] = 3;
		game.player++;
		roll.x++;
	}
	//
	do {
		game.player = 0;
		roll.x = 0;

		for (int playerNum = 1; playerNum <= game.numPlayers; playerNum++)
		{
			roll.y = (game.numPlayers - 1);
			roll.z = game.player;

			//Determine winner and display to the user.
			if (roll.chips[roll.x] + roll.center == game.chipsTotal)
			{
				game.winner = true;
				cout << "\n" << game.name[game.player] << " wins with a total of " << roll.center + roll.chips[roll.x] << " chips!" << endl;
				system("pause");
				cout << "Press any key to exit." << endl;
				return 0;
			}
			//Continue playing if there is no winner.
			else if (game.winner == false)
			{
				cout << "\n" << game.name[game.player] << " has " << roll.chips[roll.x] << " chips." << endl;
				roll.RunGame();
				game.player++;
				roll.x++;
			}
		}
	} while (game.winner == false);
}


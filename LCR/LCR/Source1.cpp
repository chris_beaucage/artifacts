#include <iostream>
#include "GameDice.h"
#include <conio.h>

using namespace std;

//Request user to roll dice.
void Dice::Rolling() {
	cout << "Press any key to roll!" << endl;
	_getch();
	cout << "Rolling dice.." << endl;
}

//Determines how many dice should be thrown, depending on the # of current player's chips.
void Dice::RunGame() {
	if (chips[x] == 1) {
		Rolling();
		Roll();
	}
	else if (chips[x] == 2) {
		Rolling();
		Roll();
		Roll();
	}
	else if (chips[x] >= 3) {
		Rolling();
		Roll();
		Roll();
		Roll();
	}
	else {
	}
}

void Dice::Roll() {
	//Simulates rolling a dice with values 1-6.
	diceRoll = (rand() % 6) + 1;

	//Determine the dice value and pass chips to players accordingly.
	switch (diceRoll) {
		case 1:
			if (z == 0) {
				cout << "L" << endl;
				chips[0] += -1;
				chips[y] += 1;
				break;
			}
			else {
				cout << "L" << endl;
				chips[x] += -1;
				chips[x - 1] += 1;
				break;
			}
		case 2:
			if (y == z) {
				cout << "R" << endl;
				chips[y] += -1;
				chips[0] += 1;
				break;
			}
			else {
				cout << "R" << endl;
				chips[x] += -1;
				chips[x + 1] += 1;
				break;
			}
		case 3:
			cout << "C" << endl;
			chips[x] += -1;
			center += 1;
			break;
		case 4:
			cout << "*" << endl;
			break;
		case 5:
			cout << "*" << endl;
			break;
		default:
			cout << "*" << endl;
			break;
	}
}